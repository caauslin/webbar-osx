//
//  StatusMenuController.m
//  Webbar
//
//  Created by Lin on 24/07/2015.
//  Copyright (c) 2015 Lin. All rights reserved.
//

#import "MenuController.h"

@interface MenuController ()

@property (strong, nonatomic) NSStatusItem *statusItem;
@property (weak) IBOutlet NSMenu *statusMenu;
@property (weak) IBOutlet NSTextField *apacheStatusText;
@property (weak) IBOutlet NSTextField *mysqlStatusText;
@property (weak) IBOutlet NSTextField *apacheUpTimeText;
@property (weak) IBOutlet NSTextField *mysqlUpTimeText;

@property (strong, nonatomic) NSImage *statusItemActiveImage;
@property (strong, nonatomic) NSImage *statusItemInactiveImage;

@property BOOL isApacheRunning;
@property BOOL isMySQLRunning;

@property (strong, nonatomic) NSDate *apacheStartTime;
@property (strong, nonatomic) NSDate *mysqlStartTime;

@property (strong, nonatomic) NSTimer *statusUpdateTimer;

@property BOOL menuVisible;

@end

@implementation MenuController

- (void)awakeFromNib {
    self.statusItemActiveImage = [NSImage imageNamed:@"StatusItem-Active-Image"];
    self.statusItemInactiveImage = [NSImage imageNamed:@"StatusItem-Inactive-Image"];
    
    self.menuVisible = NO;
    
    [self setupStatusItem];
    [self setupStatusUpdateTimer];
    
    [self updateStatus];
    [self updateMenu];
}

- (void)setupStatusItem {
    self.statusItem = [[NSStatusBar systemStatusBar]
                       statusItemWithLength:NSVariableStatusItemLength];
    self.statusItem.image = self.statusItemInactiveImage;
    
    [self.statusItem setHighlightMode:true];
    [self.statusMenu setDelegate:self];
    self.statusItem.menu = self.statusMenu;
}

// TODO: using GCD (Grand Central Dispatch) instead of NSTimer
- (void)setupStatusUpdateTimer {
    self.statusUpdateTimer = [NSTimer
                        scheduledTimerWithTimeInterval:2.5
                        target:self
                        selector:NSSelectorFromString(@"updateStatus")
                        userInfo:nil
                        repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:self.statusUpdateTimer forMode:NSRunLoopCommonModes];
}

- (void)stopStatusUpdateTimer {
    [self.statusUpdateTimer invalidate];
    self.statusUpdateTimer = nil;
}

- (void)updateStatus {
    BOOL wasApacheRunning = self.isApacheRunning;
    BOOL wasMySQLRunning = self.isMySQLRunning;
    
    self.isApacheRunning = [[NSFileManager defaultManager] fileExistsAtPath:@"/var/run/httpd.pid"];
    self.isMySQLRunning = [[NSFileManager defaultManager] fileExistsAtPath:@"/tmp/mysql.sock"];
    
    if (!(self.isApacheRunning && wasApacheRunning) && self.isApacheRunning) {
        self.apacheStartTime = [NSDate date];
    }
    
    if (!(self.isMySQLRunning && wasMySQLRunning) && self.isMySQLRunning) {
        self.mysqlStartTime = [NSDate date];
    }
    
    if (self.isApacheRunning || self.isMySQLRunning) {
        self.statusItem.image = self.statusItemActiveImage;
    } else {
        self.statusItem.image = self.statusItemInactiveImage;
    }
    
    if (self.menuVisible) {
        [self updateMenu];
    }
}

- (void)updateMenu {
    if (self.isApacheRunning) {
        self.apacheStatusText.stringValue = @"Running";
        self.apacheUpTimeText.stringValue = [self getUpTime:self.apacheStartTime];
    } else {
        self.apacheStatusText.stringValue = @"Stopped";
        self.apacheUpTimeText.stringValue = @"--";
    }
    
    if (self.isMySQLRunning) {
        self.mysqlStatusText.stringValue = @"Running";
        self.mysqlUpTimeText.stringValue = [self getUpTime:self.mysqlStartTime];
    } else {
        self.mysqlStatusText.stringValue = @"Stopped";
        self.mysqlUpTimeText.stringValue = @"--";
    }
}

- (NSString *)getUpTime:(NSDate *) startTime {
    NSTimeInterval elapsed = -[startTime timeIntervalSinceNow];
    
    div_t h = div(elapsed, 3600);
    int hours = h.quot;
    
    div_t m = div(h.rem, 60);
    int minutes = m.quot;
    int seconds = m.rem;
    
    NSString *elapsedString = @"";
    
    if (hours > 0) {
        elapsedString = [elapsedString stringByAppendingFormat:@"%dh", hours];
    }
    if (minutes > 0 || hours > 0) {
        elapsedString = [elapsedString stringByAppendingFormat:@" %dm", minutes];
    }
    if (hours == 0) {
        elapsedString = [elapsedString stringByAppendingFormat:@" %ds", seconds];
    }
    
    return elapsedString;
}

- (void)menuWillOpen:(NSMenu *)menu {
    self.menuVisible = YES;
}

- (void)menuDidClose:(NSMenu *)menu {
    self.menuVisible = NO;
}

- (IBAction)quit:(id) sender {
    [self stopStatusUpdateTimer];
    [[NSApplication sharedApplication] terminate:nil];
}

@end
