//
//  main.m
//  Webbar
//
//  Created by Lin on 24/07/2015.
//  Copyright (c) 2015 Lin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
